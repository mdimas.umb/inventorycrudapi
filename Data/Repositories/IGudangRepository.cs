﻿using InventoryCrudApi.Models;
using Microsoft.EntityFrameworkCore;

namespace InventoryCrudApi.Data.Repositories
{
    public interface IGudangRepository
    {
        Task<List<Gudang>> GetAllAsync();
        Task<Gudang> GetByIdAsync(int id);
        Task CreateAsync(Gudang gudang);
        Task UpdateAsync(Gudang gudang);
        Task DeleteAsync(int id);
    }

    public class GudangRepository : IGudangRepository
    {
        private readonly AppDbContext _context;

        public GudangRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Gudang>> GetAllAsync()
        {
            return await _context.Gudang.ToListAsync();
        }

        public async Task<Gudang> GetByIdAsync(int id)
        {
            return await _context.Gudang.FindAsync(id);
        }

        public async Task CreateAsync(Gudang gudang)
        {
            _context.Gudang.Add(gudang);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Gudang gudang)
        {
            _context.Entry(gudang).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var gudang = await _context.Gudang.FindAsync(id);
            if (gudang != null)
            {
                _context.Gudang.Remove(gudang);
                await _context.SaveChangesAsync();
            }
        }
    }
}
