﻿using InventoryCrudApi.Data.Repositories;
using InventoryCrudApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace InventoryCrudApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GudangController : ControllerBase
    {
        private readonly IGudangRepository _repository;

        public GudangController(IGudangRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Gudang>>> GetAll()
        {
            var gudangs = await _repository.GetAllAsync();
            return Ok(gudangs);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Gudang>> GetById(int id)
        {
            var gudang = await _repository.GetByIdAsync(id);
            if (gudang == null)
            {
                return NotFound();
            }
            return Ok(gudang);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Gudang gudang)
        {
            await _repository.CreateAsync(gudang);
            return CreatedAtAction(nameof(GetById), new { id = gudang.Id }, gudang);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, Gudang gudang)
        {
            if (id != gudang.Id)
            {
                return BadRequest();
            }
            await _repository.UpdateAsync(gudang);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _repository.DeleteAsync(id);
            return NoContent();
        }
    }
}
