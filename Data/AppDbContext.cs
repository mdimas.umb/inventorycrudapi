﻿using Microsoft.EntityFrameworkCore;
using InventoryCrudApi.Models;

namespace InventoryCrudApi.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Gudang> Gudang { get; set; }
        public DbSet<Barang> Barang { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Gudang>().ToTable("gudang");
            modelBuilder.Entity<Barang>().ToTable("barang");

            // Handle nullable DateTime for ExpiredBarang property
            modelBuilder.Entity<Barang>()
                .Property(b => b.ExpiredBarang)
                .HasConversion(
                    v => v.HasValue ? v.Value.ToUniversalTime() : (DateTime?)null, // Convert to UTC if value is present
                    v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : (DateTime?)null); // Specify UTC kind if value is present


            modelBuilder.Entity<Barang>()
                .HasOne(b => b.Gudang)
                .WithMany(g => g.Barang)
                .HasForeignKey(b => b.GudangId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Barang>()
                .HasOne(b => b.Gudang)
                .WithMany()
                .HasForeignKey(b => b.KodeGudang)
                .HasPrincipalKey(g => g.KodeGudang);
        }
    }
}
