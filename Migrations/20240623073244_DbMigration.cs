﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace InventoryCrudApi.Migrations
{
    /// <inheritdoc />
    public partial class DbMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "gudang",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    KodeGudang = table.Column<string>(type: "text", nullable: false),
                    NamaGudang = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gudang", x => x.Id);
                    table.UniqueConstraint("AK_gudang_KodeGudang", x => x.KodeGudang);
                });

            migrationBuilder.CreateTable(
                name: "barang",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    kode_barang = table.Column<string>(type: "text", nullable: false),
                    nama_barang = table.Column<string>(type: "text", nullable: false),
                    harga_barang = table.Column<decimal>(type: "numeric", nullable: false),
                    jumlah_barang = table.Column<int>(type: "integer", nullable: false),
                    expired_barang = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    kode_gudang = table.Column<string>(type: "text", nullable: false),
                    id_gudang = table.Column<int>(type: "integer", nullable: true),
                    GudangId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_barang", x => x.id);
                    table.ForeignKey(
                        name: "FK_barang_gudang_GudangId",
                        column: x => x.GudangId,
                        principalTable: "gudang",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_barang_gudang_id_gudang",
                        column: x => x.id_gudang,
                        principalTable: "gudang",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_barang_gudang_kode_gudang",
                        column: x => x.kode_gudang,
                        principalTable: "gudang",
                        principalColumn: "KodeGudang",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_barang_GudangId",
                table: "barang",
                column: "GudangId");

            migrationBuilder.CreateIndex(
                name: "IX_barang_id_gudang",
                table: "barang",
                column: "id_gudang");

            migrationBuilder.CreateIndex(
                name: "IX_barang_kode_gudang",
                table: "barang",
                column: "kode_gudang");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "barang");

            migrationBuilder.DropTable(
                name: "gudang");
        }
    }
}
