﻿namespace InventoryCrudApi.Models
{
    public class MonitoringItemDto
    {
        public int Id { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public decimal HargaBarang { get; set; }
        public int JumlahBarang { get; set; }
        public DateTime? ExpiredBarang { get; set; }
        public string KodeGudang { get; set; }
        public string NamaGudang { get; set; }
    }
}
