﻿using InventoryCrudApi.Data.Repositories;
using InventoryCrudApi.Models;
using Microsoft.AspNetCore.Mvc;



namespace InventoryCrudApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BarangController : ControllerBase
    {
        private readonly IBarangRepository _barangRepository;
        private readonly IGudangRepository _gudangRepository;

        public BarangController(IBarangRepository barangRepository, IGudangRepository gudangRepository)
        {
            _barangRepository = barangRepository ?? throw new ArgumentNullException(nameof(barangRepository));
            _gudangRepository = gudangRepository ?? throw new ArgumentNullException(nameof(gudangRepository));
        }

        // GET: api/barang
        [HttpGet]
        public async Task<ActionResult<List<Barang>>> GetAllBarang()
        {
            try
            {
                var barangList = await _barangRepository.GetAllAsync(includeGudang: true);
                return Ok(barangList);
            }
            catch (Exception ex) 
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error retrieving data from database: {ex.Message}");
            }
        }

        // GET: api/barang/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Barang>> GetBarangById(int id)
        {
            try
            {
                var barang = await _barangRepository.GetByIdAsync(id, includeGudang: true);
                if (barang == null)
                {
                    return NotFound();
                }
                return Ok(barang);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error retrieving data from database: {ex.Message}");
            }
        }

        // GET: api/barang/monitoring
        [HttpGet("monitoring")]
        public async Task<ActionResult<List<MonitoringItemDto>>> GetMonitoringItems(string namaGudang, DateTime? expiredDate)
        {
            try
            {
                var monitoringItems = await _barangRepository.GetMonitoringItemsAsync(namaGudang, expiredDate);
                return Ok(monitoringItems);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error retrieving monitoring items: {ex.Message}");
            }
        }

        // POST: api/barang
        [HttpPost]
        public async Task<ActionResult<Barang>> CreateBarang(Barang barang)
        {
            try
            {
                await _barangRepository.CreateAsync(barang);
                return CreatedAtAction(nameof(GetBarangById), new { id = barang.Id }, barang);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error creating new Barang: {ex.Message}");
            }
        }

        // PUT: api/barang/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBarang(int id, Barang barang)
        {

            try
            {
                if (id != barang.Id)
            {
                return BadRequest("ID in URL does not match ID in body.");
            }
                await _barangRepository.UpdateAsync(barang);
                return Ok(barang);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error updating Barang with ID: {id}. Error: {ex.Message}");
            }
        }

        // DELETE: api/barang/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBarang(int id)
        {
            try
            {
                await _barangRepository.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error deleting Barang with ID: {id}. Error: {ex.Message}");
            }
        }

        // GET: api/barang/gudangdropdown
        [HttpGet("gudangdropdown")]
        public async Task<ActionResult<List<Gudang>>> GetGudangDropdown()
        {
            try
            {
                var gudangList = await _gudangRepository.GetAllAsync();
                return Ok(gudangList);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error retrieving Gudang dropdown data: {ex.Message}");
            }
        }
    }
}
