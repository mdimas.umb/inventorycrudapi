﻿using InventoryCrudApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InventoryCrudApi.Data.Repositories
{
    public interface IBarangRepository
    {
        Task<List<Barang>> GetAllAsync(bool includeGudang = false);
        Task<Barang> GetByIdAsync(int id, bool includeGudang = false);
        Task CreateAsync(Barang barang);
        Task UpdateAsync(Barang barang);
        Task DeleteAsync(int id);
        Task<List<MonitoringItemDto>> GetMonitoringItemsAsync(string namaGudang, DateTime? expiredDate);
    }

    public class BarangRepository : IBarangRepository
    {
        private readonly AppDbContext _context;

        public BarangRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Barang>> GetAllAsync(bool includeGudang = false)
        {
            IQueryable<Barang> query = _context.Barang;

            if (includeGudang)
            {
                query = query.Include(b => b.Gudang);
            }

            return await query.ToListAsync();
        }

        public async Task<Barang> GetByIdAsync(int id, bool includeGudang = false)
        {
            IQueryable<Barang> query = _context.Barang;

            if (includeGudang)
            {
                query = query.Include(b => b.Gudang);
            }

            return await query.FirstOrDefaultAsync(b => b.Id == id);
        }

        public async Task CreateAsync(Barang barang)
        {
            _context.Barang.Add(barang);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Barang barang)
        {
            _context.Entry(barang).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var barang = await _context.Barang.FindAsync(id);
            if (barang != null)
            {
                _context.Barang.Remove(barang);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<MonitoringItemDto>> GetMonitoringItemsAsync(string namaGudang, DateTime? expiredDate)
        {
            IQueryable<MonitoringItemDto> query = _context.Barang
                .Where(b => string.IsNullOrEmpty(namaGudang) || b.Gudang.NamaGudang == namaGudang)
                .Where(b => !expiredDate.HasValue || b.ExpiredBarang <= expiredDate.Value.ToUniversalTime())
                .Select(b => new MonitoringItemDto
                {
                    Id = b.Id,
                    KodeBarang = b.KodeBarang,
                    NamaBarang = b.NamaBarang,
                    HargaBarang = b.HargaBarang,
                    JumlahBarang = b.JumlahBarang,
                    ExpiredBarang = b.ExpiredBarang.HasValue ? DateTime.SpecifyKind(b.ExpiredBarang.Value, DateTimeKind.Utc) : (DateTime?)null,
                    KodeGudang = b.KodeGudang,
                    NamaGudang = b.Gudang.NamaGudang
                });

            return await query.ToListAsync();
        }

    }

}
