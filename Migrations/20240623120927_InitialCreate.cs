﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InventoryCrudApi.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang");

            migrationBuilder.DropForeignKey(
                name: "FK_barang_gudang_id_gudang",
                table: "barang");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_gudang_KodeGudang",
                table: "gudang");

            migrationBuilder.DropIndex(
                name: "IX_barang_id_gudang",
                table: "barang");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "gudang",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "NamaGudang",
                table: "gudang",
                newName: "nama_gudang");

            migrationBuilder.RenameColumn(
                name: "KodeGudang",
                table: "gudang",
                newName: "kode_gudang");

            migrationBuilder.AlterColumn<int>(
                name: "GudangId",
                table: "barang",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_gudang_kode_gudang",
                table: "gudang",
                column: "kode_gudang");

            migrationBuilder.AddForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang",
                column: "GudangId",
                principalTable: "gudang",
                principalColumn: "id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_gudang_kode_gudang",
                table: "gudang");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "gudang",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "nama_gudang",
                table: "gudang",
                newName: "NamaGudang");

            migrationBuilder.RenameColumn(
                name: "kode_gudang",
                table: "gudang",
                newName: "KodeGudang");

            migrationBuilder.AlterColumn<int>(
                name: "GudangId",
                table: "barang",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_gudang_KodeGudang",
                table: "gudang",
                column: "KodeGudang");

            migrationBuilder.CreateIndex(
                name: "IX_barang_id_gudang",
                table: "barang",
                column: "id_gudang");

            migrationBuilder.AddForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang",
                column: "GudangId",
                principalTable: "gudang",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_barang_gudang_id_gudang",
                table: "barang",
                column: "id_gudang",
                principalTable: "gudang",
                principalColumn: "Id");
        }
    }
}
