﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InventoryCrudApi.Migrations
{
    /// <inheritdoc />
    public partial class AddGudangIdToBarang : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang");

            migrationBuilder.RenameColumn(
                name: "GudangId",
                table: "barang",
                newName: "gudangid");

            migrationBuilder.RenameIndex(
                name: "IX_barang_GudangId",
                table: "barang",
                newName: "IX_barang_gudangid");

            migrationBuilder.AlterColumn<int>(
                name: "gudangid",
                table: "barang",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_barang_gudang_gudangid",
                table: "barang",
                column: "gudangid",
                principalTable: "gudang",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_barang_gudang_gudangid",
                table: "barang");

            migrationBuilder.RenameColumn(
                name: "gudangid",
                table: "barang",
                newName: "GudangId");

            migrationBuilder.RenameIndex(
                name: "IX_barang_gudangid",
                table: "barang",
                newName: "IX_barang_GudangId");

            migrationBuilder.AlterColumn<int>(
                name: "GudangId",
                table: "barang",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_barang_gudang_GudangId",
                table: "barang",
                column: "GudangId",
                principalTable: "gudang",
                principalColumn: "id");
        }
    }
}
