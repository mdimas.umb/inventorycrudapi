﻿using System.ComponentModel.DataAnnotations.Schema;

namespace InventoryCrudApi.Models
{
    [Table("barang")]
    public class Barang
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("kode_barang")]
        public string KodeBarang { get; set; }

        [Column("nama_barang")]
        public string NamaBarang { get; set; }

        [Column("harga_barang")]
        public decimal HargaBarang { get; set; }

        [Column("jumlah_barang")]
        public int JumlahBarang { get; set; }

        private DateTime? expiredBarang;

        [Column("expired_barang")]
        public DateTime? ExpiredBarang 
        { 
          get => expiredBarang; 
          set => expiredBarang = value?.ToUniversalTime();
        }

        [Column("kode_gudang")]
        public string KodeGudang { get; set; }

        [Column("id_gudang")]
        public int? IdGudang { get; set; }

        [Column("gudangid")]
        public int? GudangId { get; set; }

        // Navigation property
        [ForeignKey("IdGudang")]
        public Gudang Gudang { get; set; }
    }
}
