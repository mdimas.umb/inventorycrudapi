﻿using System.ComponentModel.DataAnnotations.Schema;

namespace InventoryCrudApi.Models
{
    [Table("gudang")]
    public class Gudang
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("kode_gudang")]
        public string KodeGudang { get; set; }

        [Column("nama_gudang")]
        public string NamaGudang { get; set; }

        public ICollection<Barang> Barang { get; set; }
    }
}
